//
//  LeftMenuVC.swift
//  WallpaperDemo
//
//  Created by vision on 11/9/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit
import MessageUI
struct MenuItem {
    var displayName:String! = ""
    var code:String! = ""
}

class LeftMenuVC: UIViewController,MFMailComposeViewControllerDelegate {

    var tblMenu:UITableView = UITableView()
    var selectedCode:String! = ""
    
    var arrMenu:[MenuItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black
        self.tblMenu.separatorStyle = .none
        
        arrMenu.append(MenuItem(displayName: "SUBSCRIPTION", code: "subscription"))
        arrMenu.append(MenuItem(displayName: "Live Wallpapers", code: "live"))
        arrMenu.append(MenuItem(displayName: "Static Wallpapers", code: "static"))
        arrMenu.append(MenuItem(displayName: "Create Live Wallpaper", code: "create"))
        arrMenu.append(MenuItem(displayName: "Contact Us", code: "contact"))
        arrMenu.append(MenuItem(displayName: "Rate Us", code: "rate"))
        arrMenu.append(MenuItem(displayName: "Share Our App", code: "share"))
        
        view.addSubview(tblMenu)
        tblMenu.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            
            make.centerY.equalToSuperview()
            make.height.equalTo(60*arrMenu.count)
//            make.top.equalToSuperview().offset(120)
//            make.bottom.equalToSuperview().offset(-120)

        }
        
        tblMenu.delegate = self;
        tblMenu.dataSource = self;
        
        tblMenu.isScrollEnabled = false
        selectedCode = "live"
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension LeftMenuVC:UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let menu = arrMenu[indexPath.row]
        cell.textLabel?.text = menu.displayName
        cell.textLabel?.textColor = UIColor.white
        
        cell.backgroundColor = UIColor.black
        
        if menu.code == "subscription" {
            cell.textLabel?.font = UIFont.systemFont(ofSize: 20)
            cell.textLabel?.textColor = UIColor.red
        }

        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menu = arrMenu[indexPath.row]
        
        if selectedCode == menu.code {
            self.sideMenuViewController?.hideMenuViewController()
            return
        }
        
        if menu.code == "live" {
            
            let root = HomeVC()
            let navi = UINavigationController.init(rootViewController: root)
            self.sideMenuViewController?.setContentViewController(navi, animated: true)
            self.sideMenuViewController?.hideMenuViewController()
            selectedCode = menu.code
        }else if menu.code == "static" {
            let root = StaticWallpaper()
            let navi = UINavigationController.init(rootViewController: root)
            self.sideMenuViewController?.setContentViewController(navi, animated: true)
            self.sideMenuViewController?.hideMenuViewController()
            selectedCode = menu.code

        }else if menu.code == "create" {
            let root = CreateLiveWallpaper()
            let navi = UINavigationController.init(rootViewController: root)
            self.sideMenuViewController?.setContentViewController(navi, animated: true)
            self.sideMenuViewController?.hideMenuViewController()
            selectedCode = menu.code
            
        }else if menu.code == "contact" {
            self.sideMenuViewController?.hideMenuViewController()
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }

            
        }else if menu.code == "rate" {
            self.sideMenuViewController?.hideMenuViewController()
            ApplicationHelper.openOurRateAppPage()
        }
        else if menu.code == "share"{
            self.sideMenuViewController?.hideMenuViewController()
            let text = "Download Live Wallpaper App for Free: https://itunes.apple.com/us/app/x-live-wallpapers-screen-saver/id1304172862?ls=1&mt=8"
            
            let textToShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            self.present(activityViewController, animated: true, completion: nil)
        }
        else if menu.code == "subscription"{
            let salespage = SalesPageVC()
            self.present(salespage, animated: true, completion: nil)
            self.sideMenuViewController?.hideMenuViewController()

        }
        
        
        
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["iosindia11@gmail.com"])
        mailComposerVC.setSubject("Suggestion for our app")
//        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    func showSendMailErrorAlert() {
        UIAlertController.okAlertWithmessage("Could Not Send Email", block: {
            
        })

//        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
//        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }


}
