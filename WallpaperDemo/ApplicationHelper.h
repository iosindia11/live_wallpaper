//
//  ApplicationHelper.h
//  Demo
//
//  Created by vision on 4/16/17.
//  Copyright © 2017 SK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplicationHelper : UIApplication
+(BOOL)isAppLive;
+(BOOL)isUserRateThisApp;
+(void)openOurRateAppPage;
+(void)applicationWillEnterForeground:(UIApplication *)application;
@end
