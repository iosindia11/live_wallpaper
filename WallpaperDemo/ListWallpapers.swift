//
//  ListWallpapers.swift
//  WallpaperDemo
//
//  Created by vision on 10/28/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit

protocol ListWallpapersDelegate {
 
    func items() -> [Wallpaper]
    func didSelectItem(item:Wallpaper,at indexPath:IndexPath)
}

class ListWallpapers: UIView {

    var aDelegate:ListWallpapersDelegate!
    var selectedIndex:Int?
    var selectedCell:CellWallpaper?
    
    var collectionView:UICollectionView = {
        let flow = UICollectionViewFlowLayout.init()
        flow.itemSize = CGSize.init(width: 65, height: 95)
        flow.scrollDirection = .horizontal
        let collection = UICollectionView(frame: .init(), collectionViewLayout: flow)
        collection.register(CellWallpaper.self, forCellWithReuseIdentifier: "CellWallpaper")
        return collection
    }()
    
    init(aDelegate:ListWallpapersDelegate) {
        super.init(frame: .init())
        self.aDelegate = aDelegate
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI()  {
        self.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
    }

}
extension ListWallpapers:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aDelegate?.items().count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellWallpaper", for: indexPath) as! CellWallpaper
        cell.setup()

        if let aWallpaper = aDelegate?.items()[indexPath.row]{
            cell.aWallpaper = aWallpaper
        }

        if self.selectedIndex != nil{
            if (indexPath.row == self.selectedIndex!) {
                cell.isSelectedCell = true
                selectedCell = cell
            }else{
                cell.isSelectedCell = false
            
            }
        }else{
            cell.isSelectedCell = false
        }
        
        /*
        //-------download----
        if cell.aWallpaper.isDownloaded != true{
            if cell.aWallpaper.isDownloading != true{
                //not downloaded or downloading
                cell.imgImage.alpha = 0.6
                cell.progress.isHidden = false
                print("download video " + cell.aWallpaper.large_image_url)
                cell.aWallpaper.startDownloadingContent(completion: {
                    cell.alpha = 1.0
                    cell.progress.isHidden = true

                })
            }
            //downloading
            cell.aWallpaper.blockDownloadProgress = {(progress,wallpaper)in
                if cell.aWallpaper == wallpaper {
                    cell.progress.isHidden = false
                    cell.progress.setProgress(value: CGFloat(progress), animationDuration: 0.02)
                }
            }

        }
    */

        return cell
        
    }
    
    /*
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let aCell = cell as? CellWallpaper{
            
            if aCell.aWallpaper.isDownloaded {
                //do nothing just show media
                aCell.imgImage.alpha = 1.0
                aCell.progress.isHidden = true
            }else if aCell.aWallpaper.isDownloading{

            }else{
                aCell.aWallpaper.startDownloadingContent(completion: {
                    aCell.imgImage.alpha = 1.0
                    aCell.progress.isHidden = true
                })
            }
            
            aCell.aWallpaper.blockDownloadProgress = { (progressValue:Double) in
                self.do(inMainThread: {
                    aCell.imgImage.alpha = 0.6
                    aCell.progress.isHidden = false

                    aCell.progress.setProgress(value: CGFloat(progressValue), animationDuration: 0.05)
                })
            }

            
        }
        
    }
    */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        
        if let selectedCell = selectedCell {
            selectedCell.isSelectedCell = false
        }
        selectedCell = collectionView.cellForItem(at: indexPath) as! CellWallpaper?
        selectedCell?.isSelectedCell = true
        
        if let aWallpaper = aDelegate?.items()[indexPath.row]{
            self.aDelegate.didSelectItem(item: aWallpaper, at: indexPath)
        }
        
    }
    
}
