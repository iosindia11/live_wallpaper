//
//  CreateLiveWallpaper.swift
//  WallpaperDemo
//
//  Created by vision on 11/11/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit
import PhotosUI

class CreateLiveWallpaper: UIViewController,QBImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    let btnRight = UIButton()
    let btnChooseVideo = UIButton()

    var livePhoto:PHLivePhotoView = PHLivePhotoView()

    var videoPath:String?{
        didSet{
            lblInstruction.isHidden = true
        }
    }
    var imagePath:String?
    
    var avAsset:AVAsset?
    
    var lblInstruction:UILabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = UIColor.white
        //----live photo view
        view.addSubview(livePhoto)
        livePhoto.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        livePhoto.backgroundColor = UIColor.white
        self.title = "Create"
        self.addLeftMenuControl()
        
        
        self.view.addSubview(btnRight)
        btnRight.setImage(#imageLiteral(resourceName: "btnSave"), for: .normal)
        btnRight.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(30)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        btnRight.addTarget(self, action: #selector(btnSaveTapped), for: .touchUpInside)
        
        view.addSubview(btnChooseVideo)
        btnChooseVideo.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize.init(width: 44, height: 44))
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-20)
        }
//        btnChooseVideo.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        btnChooseVideo.setTitle("+", for: .normal)
        btnChooseVideo.setImage(#imageLiteral(resourceName: "btnPlus"), for: .normal)
//        btnChooseVideo.titleLabel?.font = UIFont.systemFont(ofSize: 26)
//        btnChooseVideo.setTitleColor(.white, for: .normal)
        btnChooseVideo.addTarget(self, action: #selector(btnChooseVideoTapped), for: .touchUpInside)
        
        /*
        var creat_launch_count:Int = Int((SKStorage.sk_object(forKey: "creat_launch_count") as? String) ?? "") ?? 0
        creat_launch_count = creat_launch_count + 1
        SKStorage.sk_setObject(String(creat_launch_count), forKey: "creat_launch_count")
        
        if creat_launch_count < 5 {
            UIAlertController.okAlertWithmessage("Start creating your own Live Photo by clicking + button at bottom", block: {
                
            })
        }
        */
        view.addSubview(lblInstruction)
        lblInstruction.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsetsMake(0, 40, 0, 40))
        }
        lblInstruction.text = "Start creating your own Live Photo by clicking + button at bottom"
        lblInstruction.numberOfLines = 0
        lblInstruction.textAlignment = .center
        lblInstruction.textColor = UIColor.black
        lblInstruction.font = UIFont.systemFont(ofSize: 22)
        
    }
    
    func btnSaveTapped()  {
        if AppDel.isSubscribed() {
            self.exportLivePhoto()
        }else{
            let salespage = SalesPageVC()
            self.navigationController?.present(salespage, animated: true, completion: nil)
        }
    }
    
    func btnChooseVideoTapped()  {
        /*
        let imagePickerController = QBImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsMultipleSelection = false
        //imagePickerController.assetCollectionSubtypes = [PHAssetCollectionSubtype.smartAlbumVideos]
        
        imagePickerController.mediaType = QBImagePickerMediaType.video;
        self.present(imagePickerController, animated: true, completion: nil)
    */
        
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.mediaTypes = [kUTTypeMovie as String,kUTTypeAVIMovie as String,kUTTypeQuickTimeMovie as String]
        picker.videoQuality = .typeHigh
        picker.videoMaximumDuration = 4
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)


    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        let aURL = info["UIImagePickerControllerMediaURL"]
        
        self.avAsset = AVAsset(url: aURL as! URL)
        self.createLivePhoto()

        /*
        if let asset = PHAsset.fetchAssets(withALAssetURLs: [aURL as! URL], options: nil).firstObject{
            
            PHImageManager.default().requestAVAsset(forVideo: asset, options: nil, resultHandler: { (aAsset, audioMix, nil) in
                
                self.avAsset = aAsset
                self.createLivePhoto()
            })

        }*/
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        
        imagePickerController.dismiss(animated: true, completion: nil)
        
        if let asset:PHAsset = assets.first as? PHAsset{
           
            PHImageManager.default().requestAVAsset(forVideo: asset, options: nil, resultHandler: { (aAsset, audioMix, nil) in
                
                self.avAsset = aAsset
                self.createLivePhoto()
            })
            
        }
    }
    
    func createLivePhoto()  {
        
        
        if let asset = self.avAsset {
            SVProgressHUD.show()

            LivePhotoMaker.manager().generateImageAndVideo(fromVideoAsset: asset, completion: { (livePhoto,aVideoPath,aImagePath) in
                
                SVProgressHUD.dismiss()
                self.videoPath = aVideoPath
                self.imagePath = aImagePath
                self.livePhoto.livePhoto = livePhoto
                self.livePhoto.startPlayback(with: .full)
            })
        }
    }
    
    func exportLivePhoto () {
        SVProgressHUD.show()
        
        PHPhotoLibrary.shared().performChanges({ () -> Void in
            let creationRequest = PHAssetCreationRequest.forAsset()
            let options = PHAssetResourceCreationOptions()
            
            if let aVideoPath = self.videoPath, let aImagePath = self.imagePath{
                let path1 = URL(fileURLWithPath: aVideoPath)
                let path2 = URL(fileURLWithPath: aImagePath)
                
                creationRequest.addResource(with: PHAssetResourceType.pairedVideo, fileURL: path1, options: options)
                creationRequest.addResource(with: PHAssetResourceType.photo, fileURL: path2, options: options)
            
            }
            SVProgressHUD.dismiss()
            
        }, completionHandler: { (success, error) -> Void in
            SVProgressHUD.dismiss()
            if success == true{
                UIAlertController.okAlertWithmessage("Live wallpaper saved to your photo library", block: {
                    
                })
            }else{
                //                UIAlertController.okAlertWithmessage("Something went wrong\nPlease try again", block: {
                //
                //                })
            }
            
        })
        
        
        
    }

}
extension CreateLiveWallpaper{

    func setupUI()  {
        
    }
}
