//
//  PhotoCell.h
//  XWallpapers
//
//  Created by Nirav Patel on 04/11/17.
//  Copyright © 2017 YellowPeople. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgWallpapers;

@end
