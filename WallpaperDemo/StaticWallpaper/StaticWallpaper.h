//
//  StaticWallpaper.h
//  WallpaperDemo
//
//  Created by vision on 11/9/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface StaticWallpaper : UIViewController{
    NSMutableArray *arrCat;
    BOOL isSideViewOpen;
    NSString *strType;
    NSMutableDictionary *dicCnt;
}
@property(nonatomic, strong) GADInterstitial *interstitial;
@property (strong, nonatomic) IBOutlet UICollectionView *colPhotos;
@property (strong, nonatomic) UIView *vwPreview;
@property (strong, nonatomic) IBOutlet UIButton *btnDownload;
@property (strong, nonatomic) IBOutlet UIButton *btnPreview;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIImageView *imgBig;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

- (IBAction)downloadAction:(id)sender;
- (IBAction)expandAction:(id)sender;
- (IBAction)editPhoto:(id)sender;


@end
