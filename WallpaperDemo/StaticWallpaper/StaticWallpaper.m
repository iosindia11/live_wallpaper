//
//  StaticWallpaper.m
//  WallpaperDemo
//
//  Created by vision on 11/9/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

#import "StaticWallpaper.h"
#import "PhotoCell.h"
#import "WallpaperDemo-Swift.h"
#define STATIC_BASE_URL @"http://onlinesocialfriends.com/nirav_new/LiveWallpapers/StaticWallpapers"
//"http://onlinesocialfriends.com/nirav_new/LiveWallpapers/"
@interface StaticWallpaper ()<UICollectionViewDataSource,UICollectionViewDelegate,CLImageEditorDelegate,GADInterstitialDelegate>
{


}
@end

@implementation StaticWallpaper

- (void)viewDidLoad {
    
    strType = @"Static";
    
    


    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = true;
    [self addLeftMenuControl];
    
    self.interstitial = [self createAndLoadInterstitial];

}
-(void)viewDidAppear:(BOOL)animated{

    if(!_colPhotos){
        _vwPreview = [[UIView alloc] initWithFrame:self.view.bounds];
        [self.view addSubview:_vwPreview];
        
        _imgBig = [[UIImageView alloc] initWithFrame:self.view.bounds];
        [_vwPreview addSubview:_imgBig];
        UITapGestureRecognizer *tapOnImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImage:)];
        tapOnImage.numberOfTapsRequired = 1;
        [_imgBig addGestureRecognizer:tapOnImage];
        [self.view sendSubviewToBack:_vwPreview];
        
        _imgBig.userInteractionEnabled = true;
        
        UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(121, 164);
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        _colPhotos = [[UICollectionView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-164, self.view.frame.size.width, 164) collectionViewLayout:flowLayout];
        [_colPhotos registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        _colPhotos.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_colPhotos];
        [self PostJson];
        
        _btnPreview = [[UIButton alloc] initWithFrame:CGRectMake(10, _colPhotos.frame.origin.y - 50, 44, 44)];
        [_btnPreview setImage:[UIImage imageNamed:@"btnPreview"] forState:UIControlStateNormal];
        [_btnPreview addTarget:self action:@selector(expandAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_btnPreview];
        
        _btnDownload = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 50, _colPhotos.frame.origin.y - 50, 44, 44)];
        [_btnDownload setImage:[UIImage imageNamed:@"btnSave"] forState:UIControlStateNormal];
        [_btnDownload addTarget:self action:@selector(downloadAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_btnDownload];
        
        _btnEdit = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2) - 25, _colPhotos.frame.origin.y - 50, 44, 44)];
        [_btnEdit setImage:[UIImage imageNamed:@"btnEdit"] forState:UIControlStateNormal];
        [_btnEdit addTarget:self action:@selector(editPhoto:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_btnEdit];
    }

}
-(void)PostJson {
    
    __block NSMutableDictionary *resultsDictionary;
    
    NSURL* url = [NSURL URLWithString:@"http://onlinesocialfriends.com/nirav_new/LiveProfile_api.php"];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setHTTPMethod:@"GET"];//use POST
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    __block NSError *error1 = [[NSError alloc] init];
    
    //use async way to connect network
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
     {
         if ([data length]>0 && error == nil) {
             resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
             
             [self doInMainThread:^{
                 dicCnt = resultsDictionary;
                 NSLog(@"dicCnt is %@",dicCnt);
                 NSString *countString = [NSString stringWithFormat:@"%@",[dicCnt valueForKey:@"count1"]];
                 
                 NSURL *urlImg = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@_%@_big.jpg",STATIC_BASE_URL,countString,strType]];
                 [_imgBig sd_setShowActivityIndicatorView:YES];
                 [_imgBig sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
                 [_imgBig sd_setImageWithURL:urlImg placeholderImage:[UIImage imageNamed:@""]];
                 
                 _colPhotos.delegate = self;
                 _colPhotos.dataSource = self;
                 
                 [_colPhotos reloadData];

             }];
         }
         else {//if( error!=nil) {
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error!" message:@"Please reopen our app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
             
         }
     }];
    //    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - CollectionView Delegate  Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    int cnt = (int)[[dicCnt valueForKey:@"count1"] integerValue];
    return cnt ;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(121, 164);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    int cnt = (int)[[dicCnt valueForKey:@"count1"] integerValue];
//    PhotoCell *cell = (PhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"photo_cell" forIndexPath:indexPath];
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSURL *urlImg = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Thumb/%ld_%@_Thumb.jpg",STATIC_BASE_URL,(cnt-indexPath.row),strType]];
    
    UIImageView *imgThumb = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 121, 164)];
    [imgThumb sd_setShowActivityIndicatorView:YES];
    [imgThumb sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [imgThumb sd_setImageWithURL:urlImg placeholderImage:[UIImage imageNamed:@""]];
    
    [cell.contentView addSubview:imgThumb];
//    [cell.imgWallpapers sd_setShowActivityIndicatorView:YES];
//    [cell.imgWallpapers sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    
//    [cell.imgWallpapers sd_setImageWithURL:urlImg placeholderImage:[UIImage imageNamed:@""]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDel.tapCountForAd += 1;

    if ([self shouldShowAd]) {
        [self showAd];
    }
    
    //ftp://onlinorq@onlinesocialfriends.com/public_html/nirav_new/LiveWallpapers/StaticWallpapers/1_Static_big.jpg
    int cnt = (int)[[dicCnt valueForKey:@"count1"] integerValue];
    NSURL *urlImg = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%ld_%@_big.jpg",STATIC_BASE_URL,cnt-indexPath.row,strType]];
    [_imgBig sd_setImageWithURL:urlImg placeholderImage:[UIImage imageNamed:@""]];

    if((cnt-indexPath.row) > 2){
//        if (![[SalesManager sharedInstance] isProductPurchased:K_AutoRenew_3day]) {
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            PurchaseVC *objVC = (PurchaseVC *)[storyboard instantiateViewControllerWithIdentifier:@"purchase_vc"];
//            [self.navigationController presentViewController:objVC animated:YES completion:^{
//            }];
//        }
//        else{
//            NSURL *urlImg = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%ld_%@_big.jpg",BASE_URL,cnt-indexPath.row,strType]];
//            [_imgBig sd_setImageWithURL:urlImg placeholderImage:[UIImage imageNamed:@""]];
//        }
    }
  //  [self lodAds];
}
- (IBAction)downloadAction:(id)sender {
    
    AppDelegate *appDel = [[UIApplication sharedApplication] delegate];
    
    if (appDel.isSubscribed == true) {
        UIImageWriteToSavedPhotosAlbum(_imgBig.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }else{
        SalesPageVC *sales = [[SalesPageVC alloc] init];
        [self presentViewController:sales animated:true completion:nil];
    }
    
    
}
- (void)image:(UIImage *)image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    if (!error)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Success"
                                     message:@"The picture has been saved successfully to your Camera Roll."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Great!!"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success!" message:@"The picture has been saved successfully to your Camera Roll." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
    }
}

- (IBAction)expandAction:(id)sender {
    _btnDownload.hidden = YES;
    _btnPreview.hidden = YES;
    _colPhotos.hidden = YES;
    _btnEdit.hidden =YES;
}
-(void)tappedImage:(id)sender{
    _btnEdit.hidden = NO;
    _btnDownload.hidden = NO;
    _btnPreview.hidden = NO;
    _colPhotos.hidden = NO;
}


- (IBAction)editPhoto:(id)sender {
    CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:_imgBig.image];
    editor.delegate = self;
    
    [self presentViewController:editor animated:YES completion:nil];
}
#pragma mark- CLImageEditor delegate

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image
{
    _imgBig.image = image;
    [editor dismissViewControllerAnimated:YES completion:nil];
}

- (GADInterstitial*)createAndLoadInterstitial {
    GADInterstitial *inter =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-4490497637669027/2168494506"];
    
    inter.delegate = self;

    GADRequest *request = [GADRequest request];
    // Request test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made.
    [inter loadRequest:request];
    
    return inter;
    
}
-(void)interstitialDidDismissScreen:(GADInterstitial *)ad{
    self.interstitial = [self createAndLoadInterstitial];
}
-(void)showAd{
    
    if (self.interstitial.isReady == true) {
        [self.interstitial presentFromRootViewController:self];
    }
    
}

-(BOOL)shouldShowAd{
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    if (appDel.isSubscribed == false) {
        if ((appDel.tapCountForAd % 3) == 0) {
            return true;
        }
    }
    return false;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
