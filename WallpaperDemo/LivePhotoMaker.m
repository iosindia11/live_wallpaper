//
//  LivePhotoMaker.m
//  WallpaperDemo
//
//  Created by vision on 10/30/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

#import "LivePhotoMaker.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "WallpaperDemo-Swift.h"

static LivePhotoMaker *manager = nil;

@implementation LivePhotoMaker

+(LivePhotoMaker *)manager
{
    @synchronized(self)
    {
        if (manager == nil)
        {
            manager = [[LivePhotoMaker alloc] init];
        }
    }
    return manager;
}
- (void)generateLivePhotoFrom:(NSString*)imagePath VideoPath:(NSString*)videoPath completion:(void(^)(NSString*,NSString*))completion {
    
    // Generate UUID
    NSString *genUUID = [[NSUUID UUID] UUIDString];
    
    
    NSString  *moviePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:videoPath.lastPathComponent];
    NSString *photoPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:imagePath.lastPathComponent];
    
    moviePath = PATH_MOVIE_FILE;
    photoPath = PATH_IMAGE_FILE;
    
    // Remove temporary files
    [self removeFile:moviePath];
    [self removeFile:photoPath];

    do {
        // Generate image.
        {
            UIImage *image = [[UIImage alloc] initWithContentsOfFile:imagePath];
            CGImageRef ref  = image.CGImage;
            
            NSMutableDictionary *metadata = [NSMutableDictionary dictionary];
            
            NSDictionary *kFigAppleMakerNote_AssetIdentifier = [NSDictionary dictionaryWithObject:genUUID forKey:@"17"];
            [metadata setObject:kFigAppleMakerNote_AssetIdentifier forKey:@"{MakerApple}"];
            
            NSMutableData *imageData = [[NSMutableData alloc] init];
            CGImageDestinationRef dest = CGImageDestinationCreateWithData((CFMutableDataRef)imageData, kUTTypeJPEG, 1, nil);
            CGImageDestinationAddImage(dest, ref, (CFDictionaryRef)metadata);
            CGImageDestinationFinalize(dest);
            
            [imageData writeToFile:photoPath atomically:YES];

        }
        
        // Generate movie.
        {
            

            
            // Second. add metadata to movie.
            QuickTimeMov *qtmov = [[QuickTimeMov alloc] initWithPath:videoPath];
            
            //QuickTimeMov *qtmov = [[QuickTimeMov alloc] initWithPath:[[NSBundle mainBundle] pathForResource:@"344" ofType:@"mov"]];
            [qtmov write:moviePath assetIdentifier:genUUID];
            
            if (completion) {
                completion(moviePath,
                           photoPath);
            }

        }
        
    } while (NO);
    

}

-(BOOL)removeFile:(NSString *)path
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:path])
    {
        NSError *error = nil;
        [fm removeItemAtPath:path error:&error];
        if(error)
        {
            NSLog(@"remove error: %@", error);
            return NO;
        }
    }
    
    return YES;
}

-(void)getMaximumCount:(BlockAPI)blockCompletion{

    if (self.isAppiCalled) {
        if (blockCompletion) {
            blockCompletion();
        }
        return;
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:@"http://onlinesocialfriends.com/nirav_new/LiveProfile_api.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            //initial count
            
            if (![SKStorage sk_objectForKey:@"count"]) {
                [SKStorage sk_setObject:@"20" forKey:@"count"];
            }

            if (blockCompletion) {
                blockCompletion();
            }
        } else {
            NSLog(@"%@ %@", response, responseObject);
            if ([responseObject objectForKey:@"count"]) {

                [SKStorage sk_setObject:[responseObject objectForKey:@"count"] forKey:@"count"];
            }
            self.isAppiCalled = true;

            if (blockCompletion) {
                blockCompletion();
            }
        }
    }];
    [dataTask resume];
}


-(void)generateImageAndVideoFromVideoAsset:(AVAsset *)asset completion:(void(^)(PHLivePhoto*,NSString*,NSString*))completion{
    CGSize size = [UIApplication sharedApplication].keyWindow.frame.size;

    NSString *uuid = [[NSUUID UUID] UUIDString];

    [self generateStillImage:0 url:(AVURLAsset *)asset withUUID:uuid];
    [self videoWith:asset withUUID:uuid];
    
    [PHLivePhoto requestLivePhotoWithResourceFileURLs:@[[NSURL fileURLWithPath:PATH_MOVIE_FILE_Create],[NSURL fileURLWithPath:PATH_IMAGE_FILE_Create]]
                                     placeholderImage:nil
                                           targetSize:size
                                          contentMode:PHImageContentModeDefault
                                        resultHandler:^(PHLivePhoto * _Nullable livePhoto, NSDictionary * _Nonnull info) {
                                            
                                            if (completion) {
                                                completion(livePhoto,PATH_MOVIE_FILE_Create,PATH_IMAGE_FILE_Create);
                                            }
                                        }];

    /*
    [self generateLivePhotoFrom:PATH_IMAGE_FILE_Create VideoPath:PATH_MOVIE_FILE_Create completion:^(NSString *video, NSString *image) {
        
        NSLog(@"video %@",video);
        NSLog(@"image %@",image);
        [PHLivePhoto requestLivePhotoWithResourceFileURLs:@[[NSURL fileURLWithPath:video],[NSURL fileURLWithPath:image]]
                                         placeholderImage:nil
                                               targetSize:size
                                              contentMode:PHImageContentModeDefault
                                            resultHandler:^(PHLivePhoto * _Nullable livePhoto, NSDictionary * _Nonnull info) {
                                                
                                                if (completion) {
                                                    completion(livePhoto);
                                                }
                                            }];

        
    }];*/
}

- (BOOL)generateStillImage:(float)time url:(AVURLAsset *)asset withUUID:(NSString*)uuid
{
    AVAssetImageGenerator *imageGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    [imageGen setAppliesPreferredTrackTransform:YES];
    imageGen.requestedTimeToleranceBefore = kCMTimeZero;
    imageGen.requestedTimeToleranceAfter = kCMTimeZero;
    NSError *error = nil;
    CMTime cutPoint = CMTimeMakeWithSeconds(time, NSEC_PER_SEC);
    
    CGImageRef ref = [imageGen copyCGImageAtTime:cutPoint actualTime:nil error:&error];
    
    //test
    //    UIImage *image = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"344" ofType:@"jpg"]];
    //    CGImageRef ref  = image.CGImage;
    
    if(error) return NO;
    
    NSMutableDictionary *metadata = [NSMutableDictionary dictionary];
    
    NSDictionary *kFigAppleMakerNote_AssetIdentifier = [NSDictionary dictionaryWithObject:uuid forKey:@"17"];
    [metadata setObject:kFigAppleMakerNote_AssetIdentifier forKey:@"{MakerApple}"];
    
    NSMutableData *imageData = [[NSMutableData alloc] init];
    CGImageDestinationRef dest = CGImageDestinationCreateWithData((CFMutableDataRef)imageData, kUTTypeJPEG, 1, nil);
    CGImageDestinationAddImage(dest, ref, (CFDictionaryRef)metadata);
    CGImageDestinationFinalize(dest);
    
    [imageData writeToFile:PATH_IMAGE_FILE_Create atomically:YES];
    
    return YES;
}

-(void)videoWith:(AVAsset *)asset withUUID:(NSString*)uuid{

    {
        // Remove temporary files
        [self removeFile:PATH_TEMP_FILE_Create];
        [self removeFile:PATH_MOVIE_FILE_Create];
        
        // First, save movie to app storage.
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
        exportSession.outputFileType = [[exportSession supportedFileTypes] objectAtIndex:0];
        
        // NSLog(@"%@", [[exportSession supportedFileTypes] description]);
        
        exportSession.outputURL = [NSURL fileURLWithPath:PATH_TEMP_FILE_Create];
        
        dispatch_semaphore_t __block semaphore = dispatch_semaphore_create(0);
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                NSLog(@"export session completed");
            } else {
                NSLog(@"export session error: %@", exportSession.error);
            }
            dispatch_semaphore_signal(semaphore);
        }];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
        
        
        // Second. add metadata to movie.
        QuickTimeMov *qtmov = [[QuickTimeMov alloc] initWithPath:PATH_TEMP_FILE_Create];
        
        //test
        //QuickTimeMov *qtmov = [[QuickTimeMov alloc] initWithPath:[[NSBundle mainBundle] pathForResource:@"344" ofType:@"mov"]];
        [qtmov write:PATH_MOVIE_FILE_Create assetIdentifier:uuid];
        
    }
}


@end
