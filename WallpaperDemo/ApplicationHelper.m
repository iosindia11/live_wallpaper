//
//  ApplicationHelper.m
//  Demo
//
//  Created by vision on 4/16/17.
//  Copyright © 2017 SK. All rights reserved.
//

#import "ApplicationHelper.h"


@implementation ApplicationHelper

+(BOOL) isAppLive{
    //test
    //return YES;
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    
    if (data){
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if ([lookup[@"resultCount"] integerValue] == 1){
            NSString* appStoreVersion = lookup[@"results"][0][@"version"];
            NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            
            if ([appStoreVersion isEqualToString:currentVersion]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppLive"];
                return YES;
            }
        }
    }
    return NO;
}
+(BOOL)isUserRateThisApp{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"isRateOurApp"];
}
+(void)openOurRateAppPage{
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromRate"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"clickedDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString * theUrl = [NSString  stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=id1304172862&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl] options:@{} completionHandler:^(BOOL success) {
        
    }];
    
}

+(void)applicationWillEnterForeground:(UIApplication *)application
{
    NSDate *dt = [[NSUserDefaults standardUserDefaults] objectForKey:@"clickedDate"];
    NSTimeInterval time = [dt timeIntervalSinceNow];
    // NSLog(@"Time Goes to:%f",-time);
    BOOL fromRate = [[NSUserDefaults standardUserDefaults] boolForKey:@"fromRate"];
    BOOL haveClicked = [[NSUserDefaults standardUserDefaults] boolForKey:@"isRateOurApp"];
    if(fromRate && !haveClicked)
    {
        if(-time < 5)
        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You didn't write good review." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//            [alert show];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromRate"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRateOurApp"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRateOurApp"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }

}

@end
