//
//  WallpaperPreview.swift
//  WallpaperDemo
//
//  Created by vision on 10/28/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//https://stackoverflow.com/questions/38106104/how-can-i-save-an-phlivephoto-to-icloud-using-cloudkit-api

import UIKit
import Photos
import PhotosUI

struct FilePaths {
    static let documentsPath : AnyObject = NSSearchPathForDirectoriesInDomains(.cachesDirectory,.userDomainMask,true)[0] as AnyObject
   static let DOCUMENT = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)

    struct VidToLive {
        static var livePath = FilePaths.documentsPath.appending("/")
    }
}

protocol WallpaperPreviewDelegate {
    
}

class WallpaperPreview: UIView,VIMVideoPlayerDelegate {
    var aDelegate:WallpaperPreviewDelegate!
    var aWallpaper:Wallpaper?
    var imgImage:UIImageView = {
        let img  = UIImageView()
        img.contentMode = .scaleAspectFill
        return img
    }()
    
    var livePhoto:PHLivePhotoView = PHLivePhotoView()
    
    
    var viewPlayer:VIMVideoPlayerView = VIMVideoPlayerView(frame: .init(x: 0, y: 0, width: 0, height: 0))
    //SFNS Display Regular
    //SFNS Display UltraLight
    //SFNS Display Bold
    //SFNS Display Thin
    var lblTime:UILabel = {
        let lbl = UILabel()
        lbl.text = "5:12"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: ".HelveticaNeueDeskInterface-Thin", size: 80)
        lbl.textColor = UIColor.white
        return lbl
    }()
    var lblDate:UILabel = {
        let lbl = UILabel()
        lbl.text = "Sunday, October 1"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: ".HelveticaNeueDeskInterface-Regular", size: 18)
        lbl.textColor = UIColor.white
        return lbl
    }()
    
    var btnSave:UIButton = {
        let btn = UIButton()
//        btn.setTitle("SAVE", for: .normal)
        btn.setImage(UIImage(named: "btnSave"), for: UIControlState.normal)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor.clear
        return btn
    }()
    var btnPreview:UIButton = {
        let btn = UIButton()
//        btn.setTitle("PREVIEW", for: .normal)
        btn.setImage(UIImage(named: "btnPreview"), for: UIControlState.normal)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor.clear
        return btn
    }()
    
    var progress:UICircularProgressRingView = UICircularProgressRingView()
    var activityIndicator:DRPLoadingSpinner = DRPLoadingSpinner()
    
    
    init(aDelegate:WallpaperPreviewDelegate) {
        super.init(frame: .init())
        self.aDelegate = aDelegate
        setupUI()
    }

    init() {
        super.init(frame: .init())
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI()  {
        /*
        addSubview(viewPlayer)
        viewPlayer.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        viewPlayer.player.isLooping = true
        viewPlayer.player.disableAirplay()
        viewPlayer.setVideoFillMode(AVLayerVideoGravityResizeAspectFill)
        viewPlayer.player.delegate = self
        
        addSubview(imgImage)
        imgImage.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        */
        
        activityIndicator.lineWidth = 3
        activityIndicator.drawCycleDuration = 0.5
        activityIndicator.drawCycleDuration = 1.0
        
        
        //----live photo view
        addSubview(livePhoto)
        livePhoto.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }

        
        //----save
        addSubview(btnSave)
        btnSave.snp.makeConstraints { (make) in
            make.width.height.equalTo(45)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-130)
        }
        
        self.btnSave.isHidden = false
        /*
        btnSave.doAfterDelay(inMain: 2, block: {
            self.btnSave.layer.cornerRadius = self.btnSave.frame.size.height / 2
            self.btnSave.clipsToBounds = true
            self.btnSave.layer.borderColor = UIColor.white.cgColor
            self.btnSave.layer.borderWidth = 2
            self.btnSave.isHidden = false
        })
        */
        
        
        //----preview
        addSubview(btnPreview)
        btnPreview.snp.makeConstraints { (make) in
            make.width.height.equalTo(45)
            make.left.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-130)
        }
        self.btnPreview.isHidden = false
        /*
        btnPreview.doAfterDelay(inMain: 2, block: {
            self.btnPreview.layer.cornerRadius = self.btnPreview.frame.size.height / 2
            self.btnPreview.clipsToBounds = true
            self.btnPreview.layer.borderColor = UIColor.white.cgColor
            self.btnPreview.layer.borderWidth = 2
            self.btnPreview.isHidden = false
        })
        */
        /*
        //----time date
        addSubview(lblTime)
        lblTime.snp.makeConstraints { (make) in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.top.equalTo(self).offset(50)
            make.height.equalTo(80)
        }
        
        addSubview(lblDate)
        lblDate.snp.makeConstraints { (make) in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.top.equalTo(lblTime.snp.bottom)
            make.height.equalTo(40)
        }
        */
        
        /*
        addSubview(progress)
        progress.snp.makeConstraints { (make) in
            make.width.equalTo(self).multipliedBy(0.35)
            make.height.equalTo(progress.snp.width)
            make.center.equalTo(self)
        }
        progress.minValue = 0.0
        progress.maxValue = 1.0
        progress.fullCircle = true
        progress.ringStyle = UICircularProgressRingStyle.ontop
        progress.outerRingWidth = 3
        progress.innerRingWidth = 3
        progress.shouldShowValueText = false
        progress.isHidden = true
        */
        
        addSubview(activityIndicator)
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: 35, height: 35)
        activityIndicator.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-60)
        }
        livePhoto.backgroundColor = UIColor.black
        activityIndicator.isHidden = true
        
        /*
        //test
        let uuid = NSUUID().uuidString

        let mov = Bundle.main.path(forResource: "344", ofType: "mov")
        let jpg = Bundle.main.path(forResource: "344", ofType: "jpg")
        
        
        let imagePreview = UIImage(contentsOfFile: jpg!)
        let image = imagePreview!
        let ref = image.cgImage
        
        let imgMetadata = ["MakerApple":["17":uuid]]
        let data = NSMutableData()
        let dest = CGImageDestinationCreateWithData(data, kUTTypePNG, 1, nil)
        CGImageDestinationAddImage(dest!, ref!, imgMetadata as CFDictionary?)
        CGImageDestinationFinalize(dest!)

        
        let path1 = FilePaths.DOCUMENT.path + "/test2.mov"
        let path2 = FilePaths.DOCUMENT.path + "/test2.jpg"
        
        print(path1)
        print(path2)
        if FileManager.default.fileExists(atPath: path1) {
            try! FileManager.default.removeItem(atPath: path1)
        }
        if FileManager.default.fileExists(atPath: path2) {
            try! FileManager.default.removeItem(atPath: path2)
        }
        
        let player = QuickTimeMov(path: mov!)
        player.write(path1, assetIdentifier: uuid)
        
        data.write(toFile: path2, atomically: true)

        PHLivePhoto.request(withResourceFileURLs: [URL.init(fileURLWithPath: path1),URL.init(fileURLWithPath: path2)],
                            placeholderImage: nil,
                            targetSize: self.bounds.size,
                            contentMode: PHImageContentMode.aspectFit,
                            resultHandler: { (aLivePhoto, info) -> Void in
                                
                                self.livePhoto.livePhoto = aLivePhoto
                                
                                self.livePhoto.startPlayback(with: .hint)
        })
        */
        
        
    }
    
    func setCurrentWallpaper(wallpaper:Wallpaper)  {
        if aWallpaper != wallpaper {
            aWallpaper = wallpaper
        }
        
        imgImage.isHidden = true
                
        
        
        //download image
        self.livePhoto.livePhoto = nil
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()

        aWallpaper?.startDownloadingContent {
            self.activityIndicator.isHidden = true
            self.showLivePhoto()
        }
        

        /*
        imgImage.alpha = 1
        viewPlayer.alpha = 0
        
        imgImage.backgroundColor = UIColor.black
//        imgImage.sd_setImage(with: URL.init(string: aWallpaper!.thumb_image_url), completed: nil)
        viewPlayer.player.setURL(URL.init(string: aWallpaper!.video_url))
        */
    }
    
    func showLivePhoto()  {
        
        if aWallpaper == nil {
            return
        }
        if aWallpaper!.video_download_path == nil {
            return
        }
        if aWallpaper!.large_image_download_path == nil {
            return
        }
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        LivePhotoMaker.manager().generateLivePhoto(from: aWallpaper!.large_image_download_path, videoPath: aWallpaper!.video_download_path!, completion: {video,image in
            
            self.aWallpaper!.video_download_path = video!
            self.aWallpaper!.large_image_download_path = image!
            let videoURL = URL(fileURLWithPath: video!)
            let imageURL = URL(fileURLWithPath: image!)
            PHLivePhoto.request(withResourceFileURLs: [videoURL,imageURL],
                                placeholderImage: nil,
                                targetSize: self.bounds.size,
                                contentMode: PHImageContentMode.aspectFit,
                                resultHandler: { (aLivePhoto, info) -> Void in
                                    
                                    self.activityIndicator.isHidden = true

                                    self.livePhoto.livePhoto = aLivePhoto
                                    
                                    self.livePhoto.startPlayback(with: .full)
            })
            

        })

    }
    func stopPlayBack() {
        if self.livePhoto != nil {
            livePhoto.stopPlayback()
        }
    }
    func showLivePhoto1()  {
        
//        var metaID:String = ""
//        let urlPath = URL.init(fileURLWithPath: aWallpaper!.video_download_path!)
//        
//        let meta = AVURLAsset(url: urlPath).metadata
//        
//        let metaData = AVAsset.init(url: urlPath)
//        
//        print(metaData.metadata)
//        
//        for md in meta {
//            
//            if (md.identifier?.contains("com.apple.quicktime.content.identifier"))! {
//                metaID = md.value as! String
//            }
//        }
//
        /*
        let uuid = NSUUID().uuidString
        let imagePreview = UIImage(contentsOfFile: self.aWallpaper!.large_image_download_path!)

        
        let urlImg = URL.init(string: aWallpaper!.large_image_download_path!)
        let dataImage = UIImagePNGRepresentation(imagePreview!)
        let image = imagePreview!
        let ref = image.cgImage
        
        let imgMetadata = ["MakerApple":["17":uuid]]
        let data = NSMutableData()
        let dest = CGImageDestinationCreateWithData(data, kUTTypePNG, 1, nil)
        CGImageDestinationAddImage(dest!, ref!, imgMetadata as CFDictionary?)
        CGImageDestinationFinalize(dest!)
        
        data.write(toFile: FilePaths.DOCUMENT.path + "/test2.jpg", atomically: true)

        let player = QuickTimeMov(path: aWallpaper!.video_download_path!)
        player.write(FilePaths.DOCUMENT.path + "/test2.mov", assetIdentifier: uuid)
        */
        
        
        
        /*
        PHLivePhoto.request(withResourceFileURLs: [URL.init(string: self.aWallpaper!.video_download_path!)!,URL.init(string: self.aWallpaper!.large_image_download_path!)!], placeholderImage: imagePreview, targetSize: self.bounds.size, contentMode: PHImageContentMode.aspectFit, resultHandler: { (livePhoto, info) -> Void in
            
            self.livePhoto.livePhoto = livePhoto
            self.livePhoto.startPlayback(with: .full)
        })
        */
        
        
//        let image = URL.init(string: self.aWallpaper!.large_image_download_path!)!.path
//        let mov = URL.init(string: self.aWallpaper!.video_download_path!)!.path
//        let output = FilePaths.DOCUMENT.path
//        let assetIdentifier = UUID().uuidString
//        let _ = try? FileManager.default.createDirectory(atPath: output, withIntermediateDirectories: true, attributes: nil)
//        do {
//            try FileManager.default.removeItem(atPath: output + "/IMG.JPG")
//            try FileManager.default.removeItem(atPath: output + "/IMG.MOV")
//            
//        } catch {
//            
//        }
        

        let path1 = URL(fileURLWithPath: self.aWallpaper!.video_download_path!)
        let path2 = URL(fileURLWithPath: self.aWallpaper!.large_image_download_path!)

        print(path1)
        print(path2)
        
        PHLivePhoto.request(withResourceFileURLs: [path1,path2],
                            placeholderImage: nil,
                            targetSize: self.bounds.size,
                            contentMode: PHImageContentMode.aspectFit,
                            resultHandler: { (aLivePhoto, info) -> Void in

                                self.livePhoto.livePhoto = aLivePhoto
                                
                                self.livePhoto.startPlayback(with: .hint)
        })


    }
    
    func videoPlayerIsReady(toPlayVideo videoPlayer: VIMVideoPlayer!) {
        print("ready")
        videoPlayer.play()
        
        self.imgImage.alpha = 0
        self.viewPlayer.alpha = 1

    }
    
    
    
    
    func exportLivePhoto () {
        PHPhotoLibrary.shared().performChanges({ () -> Void in
            let creationRequest = PHAssetCreationRequest.forAsset()
            let options = PHAssetResourceCreationOptions()
            
            let path1 = URL(fileURLWithPath: self.aWallpaper!.video_download_path!)
            let path2 = URL(fileURLWithPath: self.aWallpaper!.large_image_download_path!)

            creationRequest.addResource(with: PHAssetResourceType.pairedVideo, fileURL: path1, options: options)
            creationRequest.addResource(with: PHAssetResourceType.photo, fileURL: path2, options: options)
            
        }, completionHandler: { (success, error) -> Void in
            print(success)
            print(error)
            
        })
        
        
        
    }


}
