//
//  WallpaperVC.swift
//  WallpaperDemo
//
//  Created by vision on 11/3/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit

class WallpaperVC: UIViewController {

    var index:Int = 0
    var wallpaperPreview: WallpaperPreview = WallpaperPreview()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        
    }
    
    func setupUI()  {
        view.addSubview(wallpaperPreview)
        wallpaperPreview.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        
        
    }
    
    

    override func viewDidAppear(_ animated: Bool) {
        
        if wallpaperPreview.aWallpaper?.video_download_path == nil {
            wallpaperPreview.progress.isHidden = false
            wallpaperPreview.aWallpaper?.startDownloadingContent(completion: {
                self.wallpaperPreview.progress.isHidden = true
                self.wallpaperPreview.showLivePhoto()
                
            })
            
        }else {
            self.wallpaperPreview.progress.isHidden = true
            self.wallpaperPreview.showLivePhoto()
            
        }
        
        //downloading
        wallpaperPreview.aWallpaper?.blockDownloadProgress = {(progress,wallpaper)in
            if self.wallpaperPreview.aWallpaper! == wallpaper {
                self.wallpaperPreview.progress.isHidden = false
                self.wallpaperPreview.progress.setProgress(value: CGFloat(progress), animationDuration: 0.02)
            }
        }


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
