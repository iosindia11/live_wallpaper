//
//  AppDelegate.swift
//  WallpaperDemo
//
//  Created by vision on 10/28/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit
import Pushwoosh
import UserNotifications
import GoogleMobileAds

let AppDel = UIApplication.shared.delegate as! AppDelegate


let product_id:String = "com.LiveWallpapers.subscription"

@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate, PushNotificationDelegate {

    var window: UIWindow?
    var isAppLiveValue:Bool = false
    var isUserRateThisAppValue:Bool = false
    

    var tapCountForAd:Int = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GADMobileAds.configure(withApplicationID: "ca-app-pub-4490497637669027~1841191445")

        

        SwiftyStoreKit.completeTransactions(atomically: true, completion: { purchases in
            for purchase in purchases {
                SwiftyStoreKit.finishTransaction(purchase.transaction)
            }
        })
        
        

        self.window = UIWindow(frame: UIScreen.main.bounds)
        //FLEXManager.shared().showExplorer()
        
        /*
        let root = HomeVC()
        let navi = UINavigationController.init(rootViewController: root)
        
        self.window?.rootViewController = navi
        */

        let root = HomeVC()
        let leftMenu = LeftMenuVC()
        let navi = UINavigationController.init(rootViewController: root)

        let rootvc = AKSideMenu(contentViewController: navi, leftMenuViewController: leftMenu, rightMenuViewController: nil)
        self.window?.rootViewController = rootvc

        
        AppDel.checkSubscriptionReceipt { 
            
        }
        
        self.isAppLiveValue = ApplicationHelper.isAppLive()
        // set custom delegate for push handling, in our case AppDelegate
        PushNotificationManager.push().delegate = self
        
        // set default Pushwoosh delegate for iOS10 foreground push handling
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = PushNotificationManager.push().notificationCenterDelegate
        }
        
        // track application open statistics
        PushNotificationManager.push().sendAppOpen()
        
        // register for push notifications!
        PushNotificationManager.push().registerForPushNotifications()

        
        return true
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotificationManager.push().handlePushRegistration(deviceToken as Data!)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushNotificationManager.push().handlePushRegistrationFailure(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PushNotificationManager.push().handlePushReceived(userInfo)
        completionHandler(UIBackgroundFetchResult.noData)
    }
    func onPushReceived(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification received: \(pushNotification)")
        // shows a push is received. Implement passive reaction to a push, such as UI update or data download.
    }
    
    // this event is fired when user clicks on notification
    func onPushAccepted(_ pushManager: PushNotificationManager!, withNotification pushNotification: [AnyHashable : Any]!, onStart: Bool) {
        print("Push notification accepted: \(pushNotification)")
        // shows a user tapped the notification. Implement user interaction, such as showing push details
    }

    func checkSubscriptionReceipt(blockCompletion:BlockCompletion?)  {
        //test sandbox mode

        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "3121a9336cec4e0c8a2789fec046f45e")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, forceRefresh: false) { result in
            switch result {
            case .success(let receipt):
                print(receipt)
                let statusReceipt  = SwiftyStoreKit.verifySubscription(type: .autoRenewable, productId: product_id, inReceipt: receipt, validUntil: Date())
                
                switch statusReceipt {
                case .purchased(let expiryDate):
                    SKStorage.sk_setObject("true", forKey: "is_subscribed")
                    
                case .expired(let expiryDate):
                    SKStorage.sk_setObject("false", forKey: "is_subscribed")

                case .notPurchased:
                    SKStorage.sk_setObject("false", forKey: "is_subscribed")

                }
                /*
                SKStorage.sk_setObject("false", forKey: "is_subscribed")
                if let receiptInfos = receipt["pending_renewal_info"] as? [[String:AnyObject]]{
                    if let receiptInfo = receiptInfos.first{
                        if let status = receiptInfo["auto_renew_status"] as? String{
                            if status == "1"{
                                SKStorage.sk_setObject("true", forKey: "is_subscribed")
                            }
                        }
                    }
                }
                */
            case .error(let error):
                SKStorage.sk_setObject("false", forKey: "is_subscribed")
            }
            if let aBlock = blockCompletion{
                aBlock()
            }
        }
    }
    
    func restoreSubscription(blockCompletion:BlockCompletion?)  {
    
        SwiftyStoreKit.restorePurchases { (result) in
            self.checkSubscriptionReceipt(blockCompletion: blockCompletion)
        }
        
    }
    
    func isSubscribed() -> Bool {
        if let subscribed = SKStorage.sk_object(forKey: "is_subscribed") as? String
        {
            if subscribed == "true" {
                return true
            }
        }
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        ApplicationHelper.applicationWillEnterForeground(application)
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

