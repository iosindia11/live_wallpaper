//
//  LivePhotoMaker.h
//  WallpaperDemo
//
//  Created by vision on 10/30/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <ImageIO/ImageIO.h>
#import <Photos/Photos.h>
#import <MobileCoreServices/MobileCoreServices.h>

#define NAME_TEMP_FILE      @"tmp.mov"
#define NAME_IMAGE_FILE     @"generated.jpg"
#define NAME_MOVIE_FILE     @"generated.mov"

#define PATH_TEMP_FILE      [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:NAME_TEMP_FILE]

#define PATH_IMAGE_FILE     [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:NAME_IMAGE_FILE]
#define PATH_MOVIE_FILE     [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:NAME_MOVIE_FILE]

#define NAME_TEMP_FILE_Create      @"tmp_Create.mov"
#define NAME_IMAGE_FILE_Create     @"generated_Create.jpg"
#define NAME_MOVIE_FILE_Create     @"generated_Create.mov"

#define PATH_TEMP_FILE_Create      [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:NAME_TEMP_FILE_Create]

#define PATH_IMAGE_FILE_Create     [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:NAME_IMAGE_FILE_Create]
#define PATH_MOVIE_FILE_Create     [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:NAME_MOVIE_FILE_Create]

typedef void (^BlockAPI) (void);

@interface LivePhotoMaker : NSObject{

}
@property(assign) BOOL isAppiCalled;
+(LivePhotoMaker *)manager;
- (void)generateLivePhotoFrom:(NSString*)imagePath VideoPath:(NSString*)videoPath completion:(void(^)(NSString*,NSString*))completion;
-(void)getMaximumCount:(BlockAPI)blockCompletion;
-(void)generateImageAndVideoFromVideoAsset:(AVAsset *)asset completion:(void(^)(PHLivePhoto*,NSString*,NSString*))completion;
@end
