//
//  Wallpaper.swift
//  WallpaperDemo
//
//  Created by vision on 10/28/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit

class Wallpaper: NSObject {
    var thumb_image_url:String!
    var large_image_url:String!
    var video_url:String!

    var large_image_download_path:String?
    var video_download_path:String?

    var isDownloading:Bool = false
    var isDownloaded:Bool = false
    var blockDownloadProgress:((Double,Wallpaper) -> Void)?
    
    func startDownloadingContent(completion: ((Void) -> Void)?) {
        //download image
        
        isDownloading = true
        isDownloaded = false
        self.downloadImage(block: {
            self.downloadVideo(block: {
                self.isDownloading = false
                self.isDownloaded = true
                if let aBlock = completion{
                    aBlock()
                }
            })
        })
    }

    deinit {
        if isDownloaded == false{
            isDownloading = false
        }
    }
    func downloadImage(block: BlockCompletion?)  {
        
        
        let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        
        var fileName = URL.init(string: self.large_image_url)!.lastPathComponent
        //fileName = fileName.replacingOccurrences(of: "jpg", with: "JPG")

        let downloadPath = url.appendingPathComponent(fileName)

        if FileManager.default.fileExists(atPath: downloadPath.path){
            self.large_image_download_path = downloadPath.path
            if let aBlock = block{
                aBlock()
            }
            return
        }
        
        let manager = AFURLSessionManager.init(sessionConfiguration: URLSessionConfiguration.default)
        let request = URLRequest.init(url: URL.init(string: self.large_image_url)!)
        
        let downloadTask = manager.downloadTask(
            with: request,
            progress: { (progress: Progress) in
                
                self.do(inMainThread: { 
                    if let blockDownloadProgress = self.blockDownloadProgress{
                        blockDownloadProgress((progress.fractionCompleted / 2.0),self)
                    }
                })
                //print("Downloading... progress: \(String(describing: progress))")
        },
            
            destination: { (targetPath: URL, response: URLResponse) -> URL in
                return downloadPath
        },
            
            completionHandler: { (response: URLResponse, filePath: URL?, error: Error?) in
                self.do(inMainThread: { 
                    self.large_image_download_path = filePath?.path
                    if let aBlock = block{
                        aBlock()
                    }
                })
        }
        )
        
        downloadTask.resume()

    }
    func downloadVideo(block:BlockCompletion?)  {
        
        let url = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        
        var fileName = URL.init(string: self.video_url)!.lastPathComponent
        //fileName = fileName.replacingOccurrences(of: "mov", with: "MOV")
        let downloadPath = url.appendingPathComponent(fileName)
        
        if FileManager.default.fileExists(atPath: downloadPath.path){
            self.video_download_path = downloadPath.path
            if let aBlock = block{
                aBlock()
            }
            return
        }

        
        let manager = AFURLSessionManager.init(sessionConfiguration: URLSessionConfiguration.default)
        let request = URLRequest.init(url: URL.init(string: self.video_url)!)
        
        
        let downloadTask = manager.downloadTask(
            with: request,
            progress: { (progress: Progress) in
                //print("Downloading... progress: \(String(describing: progress))")
                self.do(inMainThread: { 
                    if let blockDownloadProgress = self.blockDownloadProgress{
                        blockDownloadProgress((0.5 + (progress.fractionCompleted / 2.0),self))
                    }
                })

        },
            
            destination: { (targetPath: URL, response: URLResponse) -> URL in
                return downloadPath
        },
            
            completionHandler: { (response: URLResponse, filePath: URL?, error: Error?) in
                self.do(inMainThread: { 
                    self.video_download_path = filePath?.path
                    if let aBlock = block{
                        aBlock()
                    }
                })
        }
        )
        
        downloadTask.resume()

    }

}
