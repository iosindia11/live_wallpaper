//
//  SalesPageVC.swift
//  WallpaperDemo
//
//  Created by vision on 11/5/17.
//  Copyright © 2017 CompanyName. All rights reserved.
/*
 unlock all contents
 get daily all features upates
 remove ads
 
 if not now clicked asked for rating
 "Please don't GO"
 
 please write us some good reviews for our app and unlock more wallpaper
- not now
- I want free wallpaper
 
 
*/
import UIKit
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

class SalesPageVC: UIViewController {

    var btnNotNow:UIButton = UIButton()
    var btnSubscribe:UIButton = UIButton()
    var btnRestore:UIButton = UIButton()
    var bgImg: UIImageView = UIImageView()
    var bgAppIcon: UIImageView = UIImageView()

    var btnInfo:UIButton = UIButton()
    let pulsator = Pulsator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        setupUI()
        
        if AppDel.isAppLiveValue == false {
            self.doAsyncMainThread {
                self.openSubscriptionInfo()
            }        
        }

    }
    
    func setupUI()  {
    /*
        End of trial and subscription renewal
        A monthly subscription (which comes with a 3-day free trial) for $19.99 USD. Your trial subscription will automatically be converted to a paid subscription unless auto-renew is turned off at least 24 hours before the end of the trial period. You will be charged $19.99 for 1 Month. Payment will be charged to iTunes Account at confirmation of purchase. Within 24 hours prior to end of the trial period your account will be charged for the subscription fee. From that moment and further, subscription automatically renews unless auto-renew is turned off at least 24 hours before the end of the current period. Any unused portion of a free trial period, if offered, will be forfeited when the user purchases a subscription to that publication, where applicable. Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase
        
        Cancelling trial or subscription
        You can turn off the auto renew for the subscription whenever you want to through iTunes. Check http://support.apple.com/kb/ht4098, when your current subscription period expires, you will be unsubscribed. The current active subscription period can not be canceled. After your subscription expires, you will no longer be able to use paid features and future updates.
        
        – Privacy Policy Link: https://docs.google.com/document/d/1LI_EdZMntUVL_FHc041Xodz9GuigsyEB-kLYxT5Rc9A/edit?usp=sharing
        
        – Terms Of Use Link: https://docs.google.com/document/d/1Pxb4FHOJNqOIrihO2VJpPh5pt0oHuzkg99koKkIp4TM/edit?usp=sharing
*/
        
        view.addSubview(bgImg)
        view.addSubview(btnNotNow)
        view.addSubview(btnSubscribe)
        view.addSubview(btnRestore)
        view.addSubview(btnInfo)
        
        //---labels
        let lbl1 = self.createLabel(text: "✔️ Unlock all contents")
        let lbl2 = self.createLabel(text: "✔️ Get all future upates")
        let lbl3 = self.createLabel(text: "✔️ Remove Ads")
      
        
        view.addSubview(lbl1)
        view.addSubview(lbl2)
        view.addSubview(lbl3)
        
        

        btnInfo.setImage(#imageLiteral(resourceName: "infoButton.png"), for: .normal)
        let color = UIColor(hexString: "#6b67fc")

        btnNotNow.setTitleColor(UIColor.black, for: .normal)
        btnSubscribe.setTitleColor(color, for: .normal)
        btnRestore.setTitleColor(color, for: .normal)
        btnNotNow.backgroundColor = UIColor.clear
        btnSubscribe.backgroundColor = UIColor.white
        btnRestore.backgroundColor = UIColor.clear
        
        btnNotNow.setTitle("Not Now", for: .normal)
        btnSubscribe.setTitle("START SUBSCRIPTION", for: .normal)
        btnRestore.setTitle("Restore", for: .normal)
        bgImg.image = UIImage(named:"bgImg.jpg")
        bgImg.contentMode = UIViewContentMode.scaleAspectFill
        
        
//        btnNotNow.layer.cornerRadius    = 8.0
//        btnSubscribe.layer.cornerRadius = 8.0
//        btnRestore.layer.cornerRadius   = 8.0
        
        btnNotNow.clipsToBounds     = true
        btnSubscribe.clipsToBounds = true
        btnRestore.clipsToBounds = true
        
        btnInfo.snp.makeConstraints { (make) in
            make.top.equalTo(35)
            make.right.equalTo(-10)
            make.width.height.equalTo(35)
        }
        btnNotNow.snp.makeConstraints { (make) in
            make.width.equalTo(120)
            make.height.equalTo(45)
            make.centerX.equalToSuperview()
            make.centerY.equalTo(btnInfo.snp.centerY)
        }
        
        
        btnRestore.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-50)
            make.width.equalTo(200)
            make.height.equalTo(45)
            make.centerX.equalToSuperview()
        }
        
        btnSubscribe.snp.makeConstraints { (make) in
            make.bottom.equalTo(btnRestore.snp.top).offset(-30)
            make.width.equalTo(self.view).multipliedBy(0.80)
            make.height.equalTo(55)
            make.centerX.equalToSuperview()
        }

        
        lbl2.snp.makeConstraints { (make) in
            make.width.equalTo(self.view).multipliedBy(0.80)
            make.height.equalTo(25)
            make.centerY.equalTo(self.view).offset(-30)
            make.centerX.equalTo(self.view)
        }
        lbl1.snp.makeConstraints { (make) in
            make.width.equalTo(self.view).multipliedBy(0.80)
            make.height.equalTo(25)
            make.bottom.equalTo(lbl2.snp.top).offset(-20)
            make.centerX.equalTo(self.view)
        }
        lbl3.snp.makeConstraints { (make) in
            make.width.equalTo(self.view).multipliedBy(0.80)
            make.height.equalTo(25)
            make.top.equalTo(lbl2.snp.bottom).offset(20)
            make.centerX.equalTo(self.view)
        }
        
        lbl1.alpha = 0.0
        lbl2.alpha = 0.0
        lbl3.alpha = 0.0
        
        self.doAfterDelay(inMain: 1, block: {
            UIView.animate(withDuration: 1, delay: 0.0, options: .curveLinear, animations: {
                lbl1.alpha = 1.0
            }, completion: { completed in
                UIView.animate(withDuration: 1, animations: {
                    lbl2.alpha = 1.0
                }, completion: { completed in
                    UIView.animate(withDuration: 1, animations: {
                        lbl3.alpha = 1.0
                    }, completion: { completed in
                        
                    })
                })
            })
        })

        //-------

        view.addSubview(bgAppIcon)
        bgAppIcon.contentMode = .scaleAspectFit
        bgAppIcon.image = #imageLiteral(resourceName: "IconApp.png")
        bgAppIcon.layer.cornerRadius = 6
        bgAppIcon.layer.masksToBounds = false
        bgAppIcon.snp.makeConstraints { (make) in
            
            make.width.height.equalTo(90)
            make.centerX.equalTo(self.view)
            make.bottom.equalTo(lbl1.snp.top).offset(-20)

        }
        

        bgImg.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        btnNotNow.addTarget(self, action: #selector(btnNotNowTapped), for: .touchUpInside)
        btnSubscribe.addTarget(self, action: #selector(btnSubscribeTapped), for: .touchUpInside)
        btnRestore.addTarget(self, action: #selector(btnRestoreTapped), for: .touchUpInside)
        btnInfo.addTarget(self, action: #selector(btnInfoTapped(sender:)), for: .touchUpInside)

        btnInfo.superview?.bringSubview(toFront: btnInfo);
        
        
        btnNotNow.isHidden = true
        self.doAfterDelay(inMain: 5, block: {
            self.btnNotNow.isHidden = false
        })
        
        /*
        pulsator.radius = 110
        pulsator.start()
        pulsator.numPulse = 3
        pulsator.backgroundColor = UIColor(red: 1, green: 1, blue: 0, alpha: 1).cgColor
        
        btnSubscribe.layer.superlayer?.insertSublayer(pulsator, below: btnSubscribe.layer)
        pulsator.isHidden = true
        */
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //pulsator.isHidden = false
        
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        //pulsator.position = btnSubscribe.center
        

    }
    func btnInfoTapped(sender:UIButton) {
        
        let actionController = SkypeActionController()
        actionController.addAction(Action("Subscription Info", style: .default, handler: { action in
            self.openSubscriptionInfo()
        }))
        actionController.addAction(Action("Privacy Policy", style: .default, handler: { action in
            self.openPrivacyPolicy()
        }))
        actionController.addAction(Action("Terms Of Use", style: .default, handler: { action in
            self.openTermsOfUse()
        }))
        actionController.addAction(Action("Cancel", style: .cancel, handler: nil))
        
        present(actionController, animated: true, completion: nil)


    }
    func btnSubscribeTapped()  {
        
        subscriptionAction()

        /*
        if AppDel.isAppLiveValue == true {
            //live
            subscriptionAction()

        }else{
            //not live
            let alert:UIAlertController = UIAlertController.alertViewController(withTitle: "Note", message: "Please make sure that you have read subscription policy carefully")
            alert.addButton(withTitle: "Subscription Policy", action: { (action:UIAlertAction) in
                self.btnInfoTapped(sender: self.btnInfo)
            })
            alert.addButton(withTitle: "Subscribe for FREE", action: { (action:UIAlertAction) in
                self.subscriptionAction()
            })
            alert.show {
                
            }
        }
        */
    }
    func subscriptionAction()  {
        SVProgressHUD.show()
        SwiftyStoreKit.purchaseProduct(product_id, completion: {(result) in
            self.doAfterDelay(inMain: 1, block: {
                AppDel.checkSubscriptionReceipt(blockCompletion: {
                    SVProgressHUD.dismiss()
                    self.dismiss(animated: true, completion: nil)
                })
            })
        })

    }
    
    func btnNotNowTapped()  {
        
        var shouldShowRateAlert:Bool = false
        
        if AppDel.isAppLiveValue {
            if ApplicationHelper.isUserRateThisApp() == false {
                shouldShowRateAlert = true
            }
        }
        
        if shouldShowRateAlert {
            let alert = UIAlertController.alertViewController(withTitle: "Please don't go!!!", message: "Write us some good review for our app and unlock more wallpaper")
            alert.addButton(withTitle: "I want free wallpaper", action: { (action:UIAlertAction) in
                ApplicationHelper.openOurRateAppPage()
            })
            alert.addButton(withTitle: "Not Now", action: { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            })
            alert.show {
                
            }
        }else{
            self.dismiss(animated: true, completion: nil)

        }
        
        
        /*
        let alertController = UIAlertController(title: "Information", message: "Select any one", preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        
        let DestructiveAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) {
            (result : UIAlertAction) -> Void in
            print("Destructive")
        }
        
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        
        let privacyAction = UIAlertAction(title: "Privacy Policy", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            let url = URL(string: "https://docs.google.com/document/d/1Pxb4FHOJNqOIrihO2VJpPh5pt0oHuzkg99koKkIp4TM/edit?usp=sharing")!
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                //If you want handle the completion block than
                UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                    print("Open url : \(success)")
                })
            }

        }
        
        let termsAction = UIAlertAction(title: "Terms OF Use(EULA)", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            let url = URL(string: "https://docs.google.com/document/d/1LI_EdZMntUVL_FHc041Xodz9GuigsyEB-kLYxT5Rc9A/edit?usp=sharing")!
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                //If you want handle the completion block than
                UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                    print("Open url : \(success)")
                })
            }
            
        }
        let subscriptionAction = UIAlertAction(title: "Subscription", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            
        }

        alertController.addAction(DestructiveAction)
        alertController.addAction(privacyAction)
        alertController.addAction(subscriptionAction)
        alertController.addAction(termsAction)
        self.present(alertController, animated: true, completion: nil)
    */
      //  self.dismiss(animated: true, completion: nil)
    }
    func btnRestoreTapped()  {
        
        SVProgressHUD.show()

        AppDel.restoreSubscription {
            SVProgressHUD.dismiss()
            if AppDel.isSubscribed() == true{
                UIAlertController.okAlertWithmessage("Your are subscribed SUCCESSFULLY", block: { 
                    self.dismiss(animated: true, completion: nil)
                })
            }else{
                UIAlertController.okAlertWithmessage("Restore subscription has been failed", block: {
                    
                })
                
            }
        }
    }
    
    
    func openTermsOfUse() {
        
        
        let webview = TOWebViewController(urlString: "https://docs.google.com/document/d/1Pxb4FHOJNqOIrihO2VJpPh5pt0oHuzkg99koKkIp4TM/edit?usp=sharing")
        webview?.showUrlWhileLoading = false
        let navi = UINavigationController(rootViewController: webview!)
        self.present(navi, animated: true, completion: nil)
 
        /*
        let url = URL(string: "https://docs.google.com/document/d/1LI_EdZMntUVL_FHc041Xodz9GuigsyEB-kLYxT5Rc9A/edit?usp=sharing")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
        */
    }
    
    func openPrivacyPolicy()  {
        
        let webview = TOWebViewController(urlString: "https://docs.google.com/document/d/1LI_EdZMntUVL_FHc041Xodz9GuigsyEB-kLYxT5Rc9A/edit?usp=sharing")
        webview?.showUrlWhileLoading = false
        let navi = UINavigationController(rootViewController: webview!)
        self.present(navi, animated: true, completion: nil)
 
        /*
        let url = URL(string: "https://docs.google.com/document/d/1Pxb4FHOJNqOIrihO2VJpPh5pt0oHuzkg99koKkIp4TM/edit?usp=sharing")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }*/

    }
    func openSubscriptionInfo()  {
        
        let path = Bundle.main.path(forResource: "subscription", ofType: "html")
        let webview = TOWebViewController(url: URL.init(fileURLWithPath: path!))
        webview?.showUrlWhileLoading = false
        let navi = UINavigationController(rootViewController: webview!)
        self.present(navi, animated: true, completion: nil)
        
        
    }
    
    func createLabel(text:String) -> UILabel {
        let lbl = UILabel()
        lbl.text = text
        
        lbl.numberOfLines = 0
        lbl.font = UIFont.systemFont(ofSize: 18)
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        
        return lbl
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
