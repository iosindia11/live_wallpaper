//
//  CellWallpaper.swift
//  WallpaperDemo
//
//  Created by vision on 10/28/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit

class CellWallpaper: UICollectionViewCell {
 
    var imgImage:UIImageView = {
        let img  = UIImageView()
        img.contentMode = .scaleAspectFill
        return img
    }()
    var progress:UICircularProgressRingView = UICircularProgressRingView()
    
    var isSelectedCell: Bool = false{
        didSet{
            self.layer.borderColor = UIColor.white.cgColor
            if isSelectedCell == false {
                self.layer.borderWidth = 0
            }else{
                self.layer.borderWidth = 3
            }
        }
    }
    var aWallpaper:Wallpaper! {
        didSet{
            imgImage.sd_setImage(with: URL.init(string: aWallpaper.thumb_image_url), completed: nil)
            
            /*
            self.imgImage.alpha = 0.6
            self.aWallpaper.startDownloadingContent(completion: {
                self.imgImage.alpha = 1.0
                self.progress.isHidden = true
            })
            */


        }
    }
    
    
    func setup()  {
        self.backgroundColor = UIColor.black

        imgImage.backgroundColor = UIColor.white.withAlphaComponent(0.6)

        addSubview(imgImage)
        imgImage.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        addSubview(progress)
        progress.snp.makeConstraints { (make) in
            make.width.equalTo(self).multipliedBy(0.35)
            make.height.equalTo(progress.snp.width)
            make.center.equalTo(self)
        }
        progress.isHidden = true
        progress.minValue = 0.0
        progress.maxValue = 1.0
        progress.fullCircle = true
        progress.ringStyle = UICircularProgressRingStyle.ontop
        progress.outerRingWidth = 3
        progress.innerRingWidth = 3
        progress.shouldShowValueText = false
    }
    
}
