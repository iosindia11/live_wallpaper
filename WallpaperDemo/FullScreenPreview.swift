//
//  FullScreenPreview.swift
//  WallpaperDemo
//
//  Created by vision on 11/4/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

class FullScreenPreview: UIViewController,PHLivePhotoViewDelegate {

    var aLivePhoto:PHLivePhoto?
    var aLivePhotoView:PHLivePhotoView = PHLivePhotoView()

    var lblTime:UILabel = {
        let lbl = UILabel()
        lbl.text = "5:12"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: ".HelveticaNeueDeskInterface-Thin", size: 80)
        lbl.textColor = UIColor.white
        return lbl
    }()
    var lblDate:UILabel = {
        let lbl = UILabel()
        lbl.text = "Sunday, October 1"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: ".HelveticaNeueDeskInterface-Regular", size: 20)
        lbl.textColor = UIColor.white
        return lbl
    }()
    
    var btnBack:UIButton = {
        let btnBack = UIButton()
        btnBack.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        btnBack.setTitle("Back", for: .normal)
        return btnBack
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(aLivePhotoView)
        aLivePhotoView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        aLivePhotoView.livePhoto = self.aLivePhoto
        aLivePhotoView.delegate = self
        
        view.addSubview(lblTime)
        lblTime.snp.makeConstraints { (make) in
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.top.equalTo(self.view).offset(90)
            make.height.equalTo(80)
        }
        
        view.addSubview(lblDate)
        lblDate.snp.makeConstraints { (make) in
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.top.equalTo(lblTime.snp.bottom)
            make.height.equalTo(40)
        }

//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnView))
//        self.view.addGestureRecognizer(tapGesture)
        
        view.addSubview(btnBack)
        btnBack.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
            make.height.equalTo(50)
        }
        btnBack.addTarget(self, action: #selector(didTapOnView), for: .touchUpInside)
        
    }
    
    func didTapOnView()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        aLivePhotoView.startPlayback(with: .full)
    }

    
    func livePhotoView(_ livePhotoView: PHLivePhotoView, willBeginPlaybackWith playbackStyle: PHLivePhotoViewPlaybackStyle) {
        
        btnBack.isHidden = true
    }
    func livePhotoView(_ livePhotoView: PHLivePhotoView, didEndPlaybackWith playbackStyle: PHLivePhotoViewPlaybackStyle) {
        
        btnBack.isHidden = false
//        livePhotoView.startPlayback(with: .full)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
