//
//  HomeVC.swift
//  WallpaperDemo
//
//  Created by vision on 10/28/17.
//  Copyright © 2017 CompanyName. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import GoogleMobileAds

class HomeVC: UIViewController,WallpaperPreviewDelegate,ListWallpapersDelegate,UIPageViewControllerDelegate,UIPageViewControllerDataSource,GADInterstitialDelegate {

    var wallpaperPreview: WallpaperPreview!
    var listWallpaper: ListWallpapers!
    var interstitial: GADInterstitial!

    var itemsSource:[Wallpaper] = []
    
    var btnLeft:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnLeft = self.addLeftMenuControl()
        
        interstitial = createAndLoadInterstitial()

        if AppDel.isSubscribed() == false{
          //  self.loadAds()
        }

        self.view.backgroundColor = UIColor.white
        
        self.listWallpaper = ListWallpapers(aDelegate: self)
        self.wallpaperPreview = WallpaperPreview(aDelegate: self)
        
        
        
        self.btnLeft.superview?.bringSubview(toFront: self.btnLeft)
        LivePhotoMaker.manager().getMaximumCount {
            self.do(inMainThread: {
                self.setupScreen()
                self.btnLeft.superview?.bringSubview(toFront: self.btnLeft)

            })
        }
        
        
        self.navigationController?.isNavigationBarHidden = true
        


    }

    
    
    func setupScreen() {
        
        let baseUrl = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/"
        
        var arrItems:[Wallpaper] = []
        
        var indexLimit:Int = 0
        if let count = SKStorage.sk_object(forKey: "count") as? Int {
            indexLimit = count
        }
        print(indexLimit)
        for index in (1...indexLimit).reversed(){
            let aWallpaper = Wallpaper()
            
            aWallpaper.thumb_image_url = baseUrl + "live_thumb/" + String(index) + "_thumb.jpg"
            aWallpaper.large_image_url = baseUrl + String(index) + "_img.jpg"
            aWallpaper.video_url = baseUrl + String(index) + "_vid.mov"
            arrItems.append(aWallpaper)
        }
        
        self.itemsSource = arrItems
        setupUI()
        
        self.wallpaperPreview.setCurrentWallpaper(wallpaper: self.itemsSource.first!)

        /*
        let aWallpaper = Wallpaper()
        aWallpaper.thumb_image_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/1_img.jpg"
        aWallpaper.large_image_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/1_img.jpg"
        aWallpaper.video_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/1_vid.mov"
        
        
        let aWallpaper2 = Wallpaper()
        aWallpaper2.thumb_image_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/2_img.jpg"
        aWallpaper2.large_image_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/2_img.jpg"
        aWallpaper2.video_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/2_vid.mov"
        
        
        let aWallpaper3 = Wallpaper()
        aWallpaper3.thumb_image_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/3_img.jpg"
        aWallpaper3.large_image_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/3_img.jpg"
        aWallpaper3.video_url = "http://onlinesocialfriends.com/nirav_new/LiveWallpapers/3_vid.mov"
        */
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupUI()  {
        
        view.addSubview(wallpaperPreview)
        wallpaperPreview.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        
        view.addSubview(listWallpaper)
        listWallpaper.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(self.view)
            make.height.equalTo(95)
        }
        
        
        wallpaperPreview.backgroundColor = UIColor.black
        listWallpaper.backgroundColor = UIColor.black
 
        wallpaperPreview.btnPreview.addTarget(self, action: #selector(btnPreviewWallpaperTapped), for: .touchUpInside)
        wallpaperPreview.btnSave.addTarget(self, action: #selector(btnSaveWallpaperTapped), for: .touchUpInside)
        
        /*
        let pagevc = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        
        let wallpapercontroller = WallpaperVC()
        wallpapercontroller.index = 0
        wallpapercontroller.wallpaperPreview.aWallpaper = self.itemsSource.first
        pagevc.setViewControllers([wallpapercontroller], direction: .forward, animated: true, completion: nil)

        pagevc.delegate = self
        pagevc.dataSource = self

        self.view.addSubview(pagevc.view)
        self.addChildViewController(pagevc)
        
        pagevc.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        */
    }
    
    //-------action
    func btnSaveWallpaperTapped()  {
        AppDel.tapCountForAd += 1
        if shouldShowAd() {
            self.loadAds()
        }
        

        if AppDel.isSubscribed() == true{
            exportLivePhoto()
        }else{
            let salespage = SalesPageVC()
            self.navigationController?.present(salespage, animated: true, completion: nil)
        }
    }
    
    func btnPreviewWallpaperTapped()  {
        //open preview controller
        AppDel.tapCountForAd += 1
        if shouldShowAd() {
            self.loadAds()
        }

        if let livePhoto = self.wallpaperPreview.livePhoto.livePhoto {
            let fullscreen = FullScreenPreview()
            fullscreen.aLivePhoto = livePhoto
            self.navigationController?.pushViewController(fullscreen, animated: true)
            
        }
     
    }
    
    func exportLivePhoto () {
        SVProgressHUD.show()

        PHPhotoLibrary.shared().performChanges({ () -> Void in
            let creationRequest = PHAssetCreationRequest.forAsset()
            let options = PHAssetResourceCreationOptions()
            
            if self.wallpaperPreview.aWallpaper != nil{
                if  self.wallpaperPreview.aWallpaper!.video_download_path != nil{
                    let path1 = URL(fileURLWithPath: self.wallpaperPreview.aWallpaper!.video_download_path!)
                    let path2 = URL(fileURLWithPath: self.wallpaperPreview.aWallpaper!.large_image_download_path!)
                    
                    creationRequest.addResource(with: PHAssetResourceType.pairedVideo, fileURL: path1, options: options)
                    creationRequest.addResource(with: PHAssetResourceType.photo, fileURL: path2, options: options)
                    
                }else {
                    SVProgressHUD.dismiss()
                }
            }else{
                SVProgressHUD.dismiss()
            }
            
        }, completionHandler: { (success, error) -> Void in
            SVProgressHUD.dismiss()
            if success == true{
                UIAlertController.okAlertWithmessage("Live wallpaper saved to your photo library", block: {
                    
                })
            }else{
//                UIAlertController.okAlertWithmessage("Something went wrong\nPlease try again", block: {
//                    
//                })
            }
            
        })
        
        
        
    }
    


    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let wallpapercontroller = WallpaperVC()
        
        if let prev = viewController as? WallpaperVC {
            if prev.index == 0 {
                return nil
            }
            wallpapercontroller.index = prev.index-1
            wallpapercontroller.wallpaperPreview.setCurrentWallpaper(wallpaper: self.itemsSource[wallpapercontroller.index])
        }
        
        return wallpapercontroller

    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let wallpapercontroller = WallpaperVC()
        
        if let prev = viewController as? WallpaperVC {
            if prev.index == self.itemsSource.count-1 {
                return nil
            }
            wallpapercontroller.index = prev.index+1
            wallpapercontroller.wallpaperPreview.setCurrentWallpaper(wallpaper: self.itemsSource[wallpapercontroller.index])
        }

        return wallpapercontroller
    }

    func items() -> [Wallpaper] {
        return self.itemsSource
    }

    func loadAds(){
        /*
        let request = GADRequest()
        interstitial.load(request)

        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-4490497637669027/2168494506")
        */
    
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }

    }
    
    func shouldShowAd() -> Bool {
        if AppDel.isSubscribed() == false{
            if ((AppDel.tapCountForAd % 3) == 0){
                return true
            }
        }

        return false
    }
    func createAndLoadInterstitial() -> GADInterstitial {
        var interstitial = GADInterstitial(adUnitID: "ca-app-pub-4490497637669027/2168494506")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    func didSelectItem(item: Wallpaper, at indexPath: IndexPath) {
        
        AppDel.tapCountForAd += 1
        if shouldShowAd() {
            self.loadAds()
        }

        self.wallpaperPreview.setCurrentWallpaper(wallpaper: item)

    }

}
