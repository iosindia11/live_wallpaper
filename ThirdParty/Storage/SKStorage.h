//
//  SKStorage.h
//  adme
//
//  Created by Sunil on 9/20/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import <Foundation/Foundation.h>

#define  key_user_id                    @"key_user_id"
#define  key_company_id                 @"key_company_id"


@interface SKStorage : NSObject

+(void)sk_setObject:(id)object forKey:(NSString*)key;
+(id)sk_objectForKey:(NSString*)key;
+(void)sk_removeObjectForKey:(NSString*)key;

//+(void)setUserObject:(id)object forKey:(NSString*)key;
//+(id)userObjectForKey:(NSString*)key;
//+(void)removeUserObjectForKey:(NSString*)key;

@end
