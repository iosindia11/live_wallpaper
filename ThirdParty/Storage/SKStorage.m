//
//  SKStorage.m
//  adme
//
//  Created by Sunil on 9/20/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import "SKStorage.h"
#import "Archiver.h"
@implementation SKStorage

+(void)sk_setObject:(id)object forKey:(NSString*)key{
    NSDictionary *dicUserData = [Archiver readFile:@"user_data"];
    NSMutableDictionary *dicUserSavedData = [[NSMutableDictionary alloc] init];
    if (dicUserData) {
        [dicUserSavedData addEntriesFromDictionary:dicUserData];
    }
    [dicUserSavedData setObject:object forKey:key];
    [Archiver createFile:dicUserSavedData fileName:@"user_data"];
}
+(id)sk_objectForKey:(NSString*)key{
    
    NSDictionary *dicUserData = [Archiver readFile:@"user_data"];
    if (dicUserData) {
        if ([dicUserData isKindOfClass:[NSDictionary class]]) {
            if ([dicUserData objectForKey:key]) {
                return [dicUserData objectForKey:key];
            }
        }
    }
    return nil;
}
+(void)sk_removeObjectForKey:(NSString*)key{
    NSDictionary *dicUserData = [Archiver readFile:@"user_data"];
    if ([dicUserData objectForKey:key]) {
        NSMutableDictionary *dicUserSavedData = [[NSMutableDictionary alloc] init];
        if (dicUserData) {
            [dicUserSavedData addEntriesFromDictionary:dicUserData];
        }
        [dicUserSavedData removeObjectForKey:key];
        [Archiver createFile:dicUserSavedData fileName:@"user_data"];
    }
}

/*
+(void)setUserObject:(id)object forKey:(NSString*)key{
    NSString *strKey =[NSString stringWithFormat:@"%@_%@",[[SharedClass manager] user_id],key];
    [SKStorage setObject:object forKey:strKey];
}
+(id)userObjectForKey:(NSString*)key{
    NSString *strKey =[NSString stringWithFormat:@"%@_%@",[[SharedClass manager] user_id],key];
    return [SKStorage objectForKey:strKey];
}
+(void)removeUserObjectForKey:(NSString*)key{
    NSString *strKey =[NSString stringWithFormat:@"%@_%@",[[SharedClass manager] user_id],key];
    return [SKStorage removeObjectForKey:strKey];
}*/

@end
