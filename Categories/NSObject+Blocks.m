//
//  NSObject+Blocks.h
//

#import "NSObject+Blocks.h"
#import <objc/runtime.h>

@implementation NSObject (Blocks)

- (void)performAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block
{
  int64_t delta = (int64_t)(1.0e9 * delay);
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

- (void)runBlock:(void (^)(void))block
{
  block();
}

- (void)doAfterDelayInMain:(NSTimeInterval)delay block:(void (^)(void))block{
    [self doAfterDelay:delay block:^{
       [self doInMainThread:^{
           block();
       }];
    }];
}

- (void)doAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block
{
  void (^block_)() = [block copy];
  [self performSelector:@selector(runBlock:) withObject:block_ afterDelay:delay];
}


- (void)doAsync:(void (^)(void))block
{
  dispatch_queue_t queue = dispatch_queue_create("simple async queue", DISPATCH_QUEUE_CONCURRENT);
  dispatch_async(queue, ^{
    @autoreleasepool {
      /// do your job
      block();
    }
  });
 
}

- (void)doAsyncMainThread:(void (^)(void))block
{
    dispatch_queue_t queue = dispatch_queue_create("simple async queue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(queue, ^{
        @autoreleasepool {
            /// do your job
            [self doInMainThread:^{
                block();
            }];
        }
    });
   
}


- (void)doInMainThread:(void (^)(void))block
{
  dispatch_async(dispatch_get_main_queue(), ^{
    block();
  });
}

- (void)doUpto:(NSInteger)times AfterDelay:(NSTimeInterval)delay block:(void (^)(NSInteger index))block
{
	for (int i=0; i<times; i++) {
		NSTimeInterval delayPerBlock=i*delay;
		[self doAfterDelay:delayPerBlock block:^{
			block(i);
		}];
	}
}



//Property
-(void)setProperty:(id)obj withKey:(NSString*)aKey{
    objc_setAssociatedObject(self, (__bridge const void *)(aKey), obj, OBJC_ASSOCIATION_RETAIN);
}
-(id)propertyWithKey:(NSString*)aKey{
    return (objc_getAssociatedObject(self, (__bridge const void *)(aKey)));
}
-(void)removePropertyWithKey:(NSString*)aKey{
	objc_removeAssociatedObjects(self);
	objc_setAssociatedObject(self, (__bridge const void *)(aKey), nil, OBJC_ASSOCIATION_RETAIN);
}


- (NSDictionary *)dictionary {
    unsigned int count = 0;
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i < count; i++) {
        
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        id value = [self valueForKey:key];
        
        if (value == nil) {
            // nothing todo
        }
        else if ([value isKindOfClass:[NSNumber class]]
                 || [value isKindOfClass:[NSString class]]
                 || [value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSMutableArray class]]) {
            // TODO: extend to other types
            [dictionary setObject:value forKey:key];
        }
        else if ([value isKindOfClass:[NSObject class]]) {
            [dictionary setObject:[value dictionary] forKey:key];
        }
        else {
            NSLog(@"Invalid type for %@ (%@)", NSStringFromClass([self class]), key);
        }
    }
    free(properties);
    return dictionary;
}


@end




