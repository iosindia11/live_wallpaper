//
//  UIView+LoadFromNib.h
//  RoadToll
//
//

@interface UIView (LoadFromNib)

- (id) initFromNibFile:(NSString *)fileName;
+(id) initFromNibFile;
@end
