//
//  UIView+LoadFromNib.m
//  RoadToll
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@implementation UIView (LoadFromNib)

- (id) initFromNibFile:(NSString *)fileName
{
 /*   id resObj = nil;
    
    if (!fileName || [fileName isEqual:@""]) {
        fileName = NSStringFromClass ([self class]);
    }
    
    NSArray *bundle = [[NSBundle mainBundle] loadNibNamed:fileName owner:self options:nil];
    
    for (id object in bundle) {
        if ([object isKindOfClass:[self class]]) {
            resObj = object;
            break;
        }
    }
    
    return resObj;
    
    */
    
    id alert;
    if (self) {
        // Initialization code
        NSArray *bundle;
        
        bundle = [[NSBundle mainBundle] loadNibNamed:fileName owner:self options:nil];
        for (id object in bundle) {
			if ([object isKindOfClass:[self class]])
				alert = object;
		}
        
    }
    return alert;

}

+(id) initFromNibFile{
	Class class = self;
	return [[class alloc] initFromNibFile:NSStringFromClass(class)];
}

@end
 
